package nl.arjendev.rearcam;

import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

class CameraPreview extends SurfaceView implements SurfaceHolder.Callback, Runnable {

	private static final String TAG = "RearCam";
	protected Context context;
	private SurfaceHolder holder;
    Thread mainLoop = null;
	private Bitmap bmp=null;

	private boolean cameraExists=false;
	private boolean shouldStop=false;
	
	// Error counter
	int errCount = 0;
	
	// /dev/videox (x=cameraId+cameraBase) is used.
	// In some omap devices, system uses /dev/video[0-3],
	// so users must use /dev/video[4-].
	// In such a case, try cameraId=0 and cameraBase=4
	private int cameraId=0;
	private int cameraBase=0;
	
	// This definition also exists in ImageProc.h.
	// Webcam must support the resolution 640x480 with YUYV format. 
	static final int IMG_WIDTH=640;
	static final int IMG_HEIGHT=480;

	// The following variables are used to draw camera images.
    private int winWidth=0;
    private int winHeight=0;
    private Rect rect;
    private int dw, dh;
    private float rate;
    
    // Activity for writing errors
    private Main holderActivity;
    
    // JNI functions
    public native int prepareCamera(int videoid);
    public native int prepareCameraWithBase(int videoid, int camerabase);
    public native int processCamera();
    public native void stopCamera();
    public native void pixeltobmp(Bitmap bitmap);
    static {
        System.loadLibrary("ImageProc");
    }
    
	public CameraPreview(Context context) {
		
		super(context);
		this.context = context;

		setFocusable(true);
		
		holder = getHolder();
		holder.addCallback(this);

	}
	
	public CameraPreview(Context context, AttributeSet attrs) {
		
		super(context, attrs);
		this.context = context;

		setFocusable(true);
		
		holder = getHolder();
		holder.addCallback(this);	
	}
	
    @Override
    public void run() {

    	int camOutput;
    	boolean abort = false;
    	
        while (true && cameraExists) {
        	//obtaining display area to draw a large image
        	if(winWidth==0){
        		winWidth=this.getWidth();
        		winHeight=this.getHeight();

        		if(winWidth*3/4<=winHeight){
        			dw = 0;
        			dh = (winHeight-winWidth*3/4)/2;
        			rate = ((float)winWidth)/IMG_WIDTH;
        			rect = new Rect(dw,dh,dw+winWidth-1,dh+winWidth*3/4-1);
        		}else{
        			dw = (winWidth-winHeight*4/3)/2;
        			dh = 0;
        			rate = ((float)winHeight)/IMG_HEIGHT;
        			rect = new Rect(dw,dh,dw+winHeight*4/3 -1,dh+winHeight-1);
        		}
        	}
        	
        	// Try obtaining a camera image (pixel data are stored in an array in JNI).
        	camOutput = processCamera();
        	
        	// Check for error (if error, value is 2)
        	if (camOutput == 2) {
        		this.errCount++;
        		
        		// 5 or more errors? Stop the stream
        		if (this.errCount >= 5) {
        			
        			// Stop thread after posting image
        			abort = true;
        			
        			// Show image on canvas
        			bmp = this.getBitmapFromAsset(holderActivity, "cam-not-found.png");
        			Log.d(TAG, "BMP Height: " + bmp.getHeight());
        			
        		}
        	}
        	else {
        		
        		// Reset error count
        		this.errCount = 0;
            	
            	// Camera image to bmp
            	pixeltobmp(bmp);
            	
        	}
        	
            Canvas canvas = getHolder().lockCanvas();
            if (canvas != null)
            {
            	// draw camera bmp on canvas
            	canvas.drawBitmap(bmp,null,rect,null);

            	getHolder().unlockCanvasAndPost(canvas);
            }
            
            if (abort == true) {
            	this.surfaceDestroyed(getHolder());
            	abort = false;
            }

            if(shouldStop){
            	shouldStop = false;  
            	break;
            }	        
        }
    }

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
	}
	
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {

		stopCamera();
		if(cameraExists){
			cameraExists = false;
			shouldStop = true;
			while(shouldStop){
				try{ 
					Thread.sleep(100); // wait for thread stopping
				}catch(Exception e){}
			}
		}
	}  
	
	public void setInterface(int iface) {
		
		// Create default bitmap
		if(bmp==null){
			bmp = Bitmap.createBitmap(IMG_WIDTH, IMG_HEIGHT, Bitmap.Config.ARGB_8888);
		}
		
		// Stop cam and thread
		if (cameraExists) {
			
			// Stop the camera
			stopCamera();
			
			// Stop the thread
			shouldStop = true;
			while (shouldStop) {
				try{ 
					Thread.sleep(100); // wait for thread stopping
				}catch(Exception e){}
			}
			
		}
		
		// Reset camera id
		this.cameraId = iface;
		
		// Set camera details
		int ret = prepareCameraWithBase(cameraId, cameraBase);
		if(ret!=-1) cameraExists = true;
		
		mainLoop = new Thread(this);
        mainLoop.start();
		
	}
	
	public Bitmap getBitmapFromAsset(Context context, String strName) {
	    AssetManager assetManager = context.getAssets();

	    InputStream istr;
	    Bitmap bitmap = null;
	    try {
	        istr = assetManager.open(strName);
	        bitmap = BitmapFactory.decodeStream(istr);
	    } catch (IOException e) {
	        return null;
	    }

	    return bitmap;
	}
	
	public void setHolderActivity(Main activity) {
		this.holderActivity = activity;
	}
	
}
