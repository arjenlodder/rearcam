package nl.arjendev.rearcam;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.stericson.RootTools.RootTools;
import com.stericson.RootTools.execution.Command;

public class Main extends Activity {
	
	CameraPreview cp;
	int selectedInterface = -1;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.main);
		
		// Set holder activity (to requests resource files)
		cp = (CameraPreview) findViewById(R.id.cp);
		cp.setHolderActivity(this);
		
		// Set refreshbutton handler
		Button refreshButton = (Button) findViewById(R.id.refreshButton);
		refreshButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Main.this.checkInterfaces();
			}
		});
		
		// Check for interfaces
		this.checkInterfaces();
		
	}
	
	public void checkInterfaces() {

		final LinearLayout buttonHolder = (LinearLayout) findViewById(R.id.buttonHolder);
		buttonHolder.removeAllViews();
		
		// Request root access
		if (RootTools.isRootAvailable()) {
			if (RootTools.isAccessGiven()) {
				
				// Check for interfaces
				Command command = new Command(0, "ls /dev/")
				{
					
					int numberFound = 0;

					@Override
					public void commandCompleted(int arg0, int arg1) {
						Toast.makeText(Main.this, numberFound + " interfaces found", Toast.LENGTH_LONG).show();
					}

					@Override
					public void commandOutput(int id, String line) {
						
						// Check if output starts with video
						if (line.matches("video[0-9]{1,}")) {
							
							// Add button to buttonHolder
							Button nButton = new Button(Main.this);
							nButton.setText("Interface " + line.replace("video", ""));
							nButton.setTag(line);
							LayoutParams params = buttonHolder.getLayoutParams();
							params.width = LayoutParams.WRAP_CONTENT;
							params.height = LayoutParams.WRAP_CONTENT;
							nButton.setPadding(60, 60, 60, 60);
							nButton.setLayoutParams(params);
							nButton.setOnClickListener(new SelectDeviceInterface());
							
							// Add button to holder
							buttonHolder.addView(nButton);
							numberFound++;
							
							// Remove errortext
							TextView errTxt = (TextView) Main.this.findViewById(R.id.errorText);
							errTxt.setText("");
							
							// Select interface if needed
							if (selectedInterface == -1) {
								nButton.performClick();
							}
							
						}
						
					}

					@Override
					public void commandTerminated(int arg0, String arg1) {
						// Nothing to do here
					}
				};
				try {
					RootTools.getShell(true).add(command);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}
		
	}
	
	public void triggerError(String err) {
		
		TextView errTxt = (TextView) Main.this.findViewById(R.id.errorText);
		errTxt.setText(err);
		
	}
	
	private class SelectDeviceInterface implements OnClickListener {

		@Override
		public void onClick(View v) {
			
			// Get video interface
			String videoInterface = ((Button)v).getTag().toString();
			final int videoNumber = Integer.parseInt(videoInterface.replace("video", ""));
			selectedInterface = videoNumber;
			
			// Set interface name
			TextView ifaceName = (TextView) findViewById(R.id.interfaceName);
			ifaceName.setText("Interface: /dev/video" + selectedInterface);
			
			// chmod interface
			if (RootTools.isRootAvailable() && RootTools.isAccessGiven()) {
				
				// Check for interfaces
				Command command = new Command(0, "chmod 777 /dev/video" + videoNumber)
				{

					@Override
					public void commandCompleted(int arg0, int arg1) {
						Toast.makeText(Main.this, "Camera activated for use", Toast.LENGTH_LONG).show();
						
						// Trigger inteface change
						cp.setInterface(videoNumber);
					}

					@Override
					public void commandOutput(int arg0, String arg1) {
						// Nothing to do here
						
					}

					@Override
					public void commandTerminated(int arg0, String arg1) {
						Toast.makeText(Main.this, "chmod niet gelukt", Toast.LENGTH_LONG).show();
					}
					
				};
				try {
					RootTools.getShell(true).add(command);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
					
			}
		}
		
	}
	
}
